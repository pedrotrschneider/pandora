class myObj extends Object2D
{
    _setup()
    {
        console.log("aaaaa");
        var area = new Area2D("area", SHAPES.RECT, new Rect(100, 100), true, true);
        area.connect("mouseEntered", this, "_onHi");
        area.connect("mouseExited", this, "_onBye");
        this.addChild(area);
    }

    _onHi()
    {
        console.log("hi");
    }

    _onBye()
    {
        console.log("Bye");
    }
}

class myTest extends Object2D
{
    _setup()
    {
        console.log("hello");
        this.addChild(new myObj("myObj"));
    }
}

GameHandler._preload = function()
{
    AssetHandler.loadTexture("monke", "/assets/textures/monke.png");
    AssetHandler.loadFont("Lato", "/assets/fonts/Lato-Regular.ttf");
    AssetHandler.loadAudio("bonk", "/assets/audio/thonk.wav");
    AssetHandler.loadAudio("music", "/assets/audio/music.ogg");
}

GameHandler._setup = function()
{
    GameHandler.drawDebugFPS(true);
    GameHandler.drawDebugBufferBounds(true);

    GameHandler.addRootObject(new myTest("test"));
}