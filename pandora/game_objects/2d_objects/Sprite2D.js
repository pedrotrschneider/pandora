/************************************************************************
 * Sprite2D.js
 ************************************************************************
 * Copyright (c) 2021 Pedro Tonini Rosenberg Schneider.
 *
 * This file is part of Pandora.
 *
 * Pandora is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pandora is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *     
 * You should have received a copy of the GNU General Public License     
 * along with Pandora.  If not, see <https://www.gnu.org/licenses/>.
 *************************************************************************/

/**
 * The {@code Sprite2D} class represents a GameObject that inherits from
 * Object2D and extends its functionality to automatically draw a Sprite on the
 * buffer.
 * 
 * ! All GameObjects need to be inside the tree to do anything (can be added as a child
 * ! of another GameObject on the tree or as a root).
 * 
 * @author Pedro Schneider
 * 
 * @class
 */
class Sprite2D extends Object2D
{
    /**
     * Initializes a Sprite2D GameObject with the specified parameters.
     * 
     * @param {String} name         name for this Sprite2D. 
     * @param {TextureRes} texture     Texture to be drawn on the buffer.
     * @param {number} imgMode      image mode to draw the p5.Image to. Default is CENTER.
     * 
     * @constructor
     */
    constructor(name, texture, imgMode = CENTER)
    {
        super(name);

        this.texture = texture; // This Sprite2D's p5.Image.
        this.imgMode = imgMode; // This Sprite2D's image mode.
        this.width = null; // This Sprite2D's width.
        this.height = null; // This Sprite2D's height.
    }

    /**
     * Applies this Object2D's transform before calling this GameObject's _draw() callback
     * and recursively calls the same callback on all of it's children. Also draws an image
     * on the buffer based on the data passed to this GameObject.
     * 
     * @param {number} delta    number in seconds ellapsed since the last frame.
     * @param {p5.Graphics} db  secondary buffer to draw to.
     * 
     * @override
     */
    draw(delta, db)
    {
        db.push();
        this.applyTransform(db);
        db.imageMode(this.imgMode);
        if (this.texture.P5Image)
        {
            if (this.width && this.height)
                db.image(this.texture.P5Image, 0, 0, this.width, this.height);
            else
                db.image(this.texture.P5Image, 0, 0, this.texture.P5Image.width, this.texture.P5Image.height);
        }
        this.drawChildren(delta, db);
        db.pop();
    }
}