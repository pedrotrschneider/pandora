/************************************************************************
 * InputHandler.js
 ************************************************************************
 * Copyright (c) 2021 Pedro Tonini Rosenberg Schneider.
 *
 * This file is part of Pandora.
 *
 * Pandora is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pandora is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *     
 * You should have received a copy of the GNU General Public License     
 * along with Pandora.  If not, see <https://www.gnu.org/licenses/>.
 *************************************************************************/

/**
 * This {@code InputHandler} singleton provides an interface for the user to
 * deal with several input events.
 * 
 * @author Pedro Schneider
 * 
 * @namespace
 */

const InputHandler = {
    mouseIsPressed: false, // Is the mouse currently pressed?
    mouseIsReleased: false, // Is the mouse currently released?
    mouseIsClicked: false, // Has the mouse been clicked? (press + release).

    update()
    {
        this.mouseIsClicked = false;
    }
}

/**
 * This function is calles once every time a mouse button is pressed over
 * the main canvas. Serves to update the InputHandler on the mouse events.
 * 
 * @callback
 */
function mousePressed()
{
    InputHandler.mouseIsPressed = true;
    InputHandler.mouseIsReleased = false;
}

/**
 * This function is called once every time a mouse button is released over
 * the main canvas. Serves to update the InputHandler on the mouse events.
 * 
 * @callback
 */
function mouseReleased()
{
    InputHandler.mouseIsReleased = true;
    InputHandler.mouseIsPressed = false;
}

/**
 * This function is called once every time a mouse button is clicked over
 * the main canvas (press + release). Serves to update the InputHandler on the
 * mouse events.
 */
function mouseClicked()
{
    InputHandler.mouseIsClicked = true;
    InputHandler.mouseIsPressed = false;
    InputHandler.mouseIsReleased = false;
}